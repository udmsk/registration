<!DOCTYPE html> 
<html>
<head>
	<title>Login & Registration</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,intial-scale=1">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="<link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,600;0,700;1,400&display=swap rel="stylesheet">
">
</head>
<body>
	<div class="cont">
		<div class="form sign-in">
			<h2>Sign In</h2>
			<label>
				<span>Email Address</span>
				<input type="email" name="email">
			</label>
			<label>
			<span>Password</span>
			<input type="Password" name="Password">
		</label>
		<button class="submit" type="button">Sign In</button>
		<p class="forgot-pass">Forgot Password ?</p>
		<div class="social-media">
			<ul>
                <li><img src="facebook.png"></li>
				<li><img src="twitter.png"></li>
				<li><img src="google.png"></li>
				<li><img src="instagram.png"></li>
			</ul>
		</div>
	</div>
	<div class="sub-cont">
		<div class="img">
			<div class="img-text m-up">
				<h2>New here?</h2>
				<p>Sign uo and discover great amount of new oppurtunities!</p>
			</div>
			<div class="img-text m-in">
				<h2>One of us?</h2>
				<p>If you already has an account , just sign in. We've missed you!</p>
			</div>
			<div class="img-btn">
				<span class="m-up">Sign Up</span>
				<span class="m-in">Sign In</   span>
		</div>
	</div>
	<div class="form sign-up">
		<h2>Sign Up</h2>
		<label>
			<span>Name</span>
			<input type="text">
		</label>
		<label>
			<span>Email</span>
			<input type="email">
		</label>
		<label>
			<span>Password</span>
			<input type="Password">
		</label>
		<label>
			<span>Confirm Password</span>
			<input type="Password">
		</label>
		<button type="button" class="submit">Sign Up Now</button>
	</div>
</div>
</div>
<script type="text/javascript" src="script.js"></script>
</body>
</html>